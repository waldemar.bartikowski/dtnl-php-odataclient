<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Request;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Tests\Mocks\MockParameter;
use \DTNL\OdataClient\Request\OdataRequestBuilder;
use DTNL\OdataClient\Tests\Mocks\MockExpression;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;

/**
 * @covers \DTNL\OdataClient\Request\OdataRequestBuilder
 * @covers \DTNL\OdataClient\Request\AbstractOdataRequestBuilder
 */
class OdataRequestBuilderTest extends TestCase {

    public function testConstruction() : void {
        $rb = new OdataRequestBuilder(
            OdataRequestInterface::GET,
            'http://localhost/service/',
            'EntityName'
        );
        
        $rb
            ->setCompute( new MockExpression( 'compute' ) )
            ->setSearch( new MockExpression( 'search' ) )
            ->setFilter( new MockExpression( 'filter' ) )
            ->setCount( true )
            ->setOrderBy( new MockExpression( 'orderby' ) )
            ->setSkip( 1 )
            ->setTop( 2 )
            ->setLevels( 3 )
            ->setExpand( new MockExpression( 'expand' ) )
            ->setSelect( new MockExpression( 'select' ) )
            ->setFormat( 'json' )
        ;

        $request = $rb->build();

        self::assertEquals( 'EntityName', $request->getResourcePath() );
        self::assertEquals( 'GET', $request->getMethod() );

        self::assertEquals(
            [
                '$compute' => 'compute',
                '$search' => 'search',
                '$filter' => 'filter',
                '$count' => 'true',
                '$orderby' => 'orderby',
                '$skip' => '1',
                '$top' => '2',
                '$levels' => '3',
                '$expand' => 'expand',
                '$select' => 'select',
                '$format' => 'json',
            ],
            $request->getParameterArray()
        );

    }
}