<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Request;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Tests\Mocks\MockParameter;
use \DTNL\OdataClient\Request\OdataRequest;
use GuzzleHttp\Psr7\Uri;
use DTNL\OdataClient\Request\StreamFactory;
use DTNL\OdataClient\Request\OdataResourcePath;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;

/**
 * @covers \DTNL\OdataClient\Request\OdataRequest
 */
class OdataRequestTest extends TestCase {

    public function testConstruction() : void {
        $request = new OdataRequest(
            OdataRequestInterface::GET,
            new Uri( 'http://localhost/service/' ),
            new OdataResourcePath( 'EntityName' )
        );
        
        self::assertEquals(
            'EntityName',
            (string) $request->getResourcePath()
        );

        self::assertEquals(
            'http://localhost/service/',
            (string) $request->getServiceRoot()
        );
    }

    public function testAddingParameter() : void {
        
        $request = new OdataRequest(
            OdataRequestInterface::GET,
            new Uri( 'http://localhost/service/' ),
            new OdataResourcePath( 'EntityName' )
        );
        
        $request->addParameter( new MockParameter() );
        $params = $request->getParameters();
        self::assertCount( 1, $params );

        $request->addParameter( new MockParameter() );
        $params = $request->getParameters();
        self::assertCount( 2, $params );
    }

    public function testGettingParamArray() : void {
        $request = new OdataRequest(
            OdataRequestInterface::GET,
            new Uri( 'http://localhost/service/' ),
            new OdataResourcePath( 'EntityName' )
        );
        
        self::assertEquals( $request->getParameterArray(), [] );

        $request->addParameter( new MockParameter() );
        self::assertEquals(
            $request->getParameterArray(),
            [ 'mock name' => 'mock expression' ]
        );

        $request->addParameter( new MockParameter() );
        self::assertEquals(
            $request->getParameterArray(),
            [
                'mock name' => [
                    'mock expression',
                    'mock expression',
                ]
            ]
        );
    }

    public function testAddingBody() : void {
        
        $request = new OdataRequest(
            OdataRequestInterface::GET,
            new Uri( 'http://localhost/service/' ),
            new OdataResourcePath( 'EntityName' )
        );

        $request->setBody(
            StreamFactory::createFromString( 'This is the body. Enjoy.' ),
            'text/text'
        );

        self::assertEquals(
            'This is the body. Enjoy.',
            (string) $request->getBody()
        );
    }
}