<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Request;

use PHPUnit\Framework\TestCase;
use DTNL\OdataClient\Request\OdataResourcePath;

/**
 * @covers \DTNL\OdataClient\Request\OdataResourcePath
 */
class OdataResourcePathTest extends TestCase {

    public function testConstruction() : void {
        $resource_path = new OdataResourcePath( 'EntityName');
        
        self::assertEquals(
            'EntityName',
            (string) $resource_path 
        );
    }
}