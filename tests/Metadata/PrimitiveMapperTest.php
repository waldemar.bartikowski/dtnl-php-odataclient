<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Metadata;

use \PHPUnit\Framework\TestCase;
use DTNL\OdataClient\Metadata\PrimitiveMapper;
use DTNL\OdataClient\Metadata\Exceptions\PrimitiveMappingException;

/**
 * @covers DTNL\OdataClient\Metadata\PrimitiveMapper
 */
class PrimitiveMapperTest extends TestCase {

    public function testBinary() : void {     
        
        self::assertEquals(
            'blob stuff',
            PrimitiveMapper::map(
                'Edm.Binary',
                base64_encode( 'blob stuff' )
            )
        );

        self::assertIsString(
            PrimitiveMapper::map( 'Edm.Binary', base64_encode( 'blob stuff' ) )
        );

        self::expectException( PrimitiveMappingException::class );
        PrimitiveMapper::map( 'Edm.Binary', "invalid base64!" );

    }

    public function testString() : void {
        self::assertEquals( 'abc', PrimitiveMapper::map( 'Edm.String', 'abc' ) );
        self::assertIsString( PrimitiveMapper::map( 'Edm.String', 'abc' ) );

        self::assertEquals( 'abc', PrimitiveMapper::map( '', 'abc' ) );
        self::assertIsString( PrimitiveMapper::map( '', 'abc' ) );

    }

    public function testInt() : void {

        self::assertEquals( 123, PrimitiveMapper::map( 'Edm.Int16', '123' ) );
        self::assertIsInt( PrimitiveMapper::map( 'Edm.Int16', '123' ) );

        self::assertEquals( 123, PrimitiveMapper::map( 'Edm.Int32', '123' ) );
        self::assertIsInt( PrimitiveMapper::map( 'Edm.Int32', '123' ) );

        self::assertEquals( 123, PrimitiveMapper::map( 'Edm.Int64', '123' ) );
        self::assertIsInt( PrimitiveMapper::map( 'Edm.Int64', '123' ) );

        self::assertEquals( 123, PrimitiveMapper::map( 'Edm.SByte', '123' ) );
        self::assertIsInt( PrimitiveMapper::map( 'Edm.SByte', '123' ) );
    }

    public function testBool() : void {     

        self::assertEquals( true, PrimitiveMapper::map( 'Edm.Boolean', '1' ) );
        self::assertEquals( true, PrimitiveMapper::map( 'Edm.Boolean', 'true' ) );
        self::assertIsBool( PrimitiveMapper::map( 'Edm.Boolean', 'true' ) );
    }

    public function testFloat() : void {     
        
        self::assertEquals( 12.32, PrimitiveMapper::map( 'Edm.Decimal', '12.32' ) );
        self::assertIsFloat( PrimitiveMapper::map( 'Edm.Decimal', '12.32' ) );

        self::assertEquals( 12.32, PrimitiveMapper::map( 'Edm.Double', '12.32' ) );
        self::assertIsFloat( PrimitiveMapper::map( 'Edm.Double', '12.32' ) );
    }

    
    public function testDate() : void {     
        
        self::assertEquals(
            new \DateTime( '2019-07-30 16:10:04Z' ),
            PrimitiveMapper::map( 'Edm.DateTimeOffset', '2019-07-30T16:10:04Z' )
        );

        self::assertEquals(
            new \DateTime( '2019-07-30 16:10:04Z' ),
            PrimitiveMapper::map( 'Edm.Date', '2019-07-30T16:10:04' )
        );
    }


}