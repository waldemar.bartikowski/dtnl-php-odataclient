<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Mocks;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class MockExpression implements ExpressionInterface {

    /** @var string */
    private $expression;

    public function __construct( string $expression = 'mock expression' ) {
        $this->expression = $expression;
    }

    public function __toString() : string {
        return $this->expression;
    }
}