<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Mocks;

use \DTNL\OdataClient\Parameter\Interfaces\ParameterInterface;
use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class MockParameter implements ParameterInterface {

    public function getName() : string {
        return 'mock name';
    }

    public function getExpression() : ExpressionInterface {
        return new MockExpression();
    }

    public function __toString() : string {
        return $this->getName() . '=' . (string) $this->getExpression();
    }
}