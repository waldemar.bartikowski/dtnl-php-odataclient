<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\AndExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\AndExpression
 */
class AndExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $se = new AndExpression(
            new MockExpression(),
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $se,
            '(mock expression and mock expression)'
        );
    }
}