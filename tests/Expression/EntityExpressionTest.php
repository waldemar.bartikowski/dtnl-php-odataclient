<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\EntityExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\EntityExpression
 */
class EntityExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $se = new EntityExpression(
            'EntityName', new MockExpression()
        );
        
        $this::assertEquals(
            (string) $se,
            'EntityName(mock expression)'
        );
    }

    public function testConstructionWithoutExpression() : void {
        
        $se = new EntityExpression( 'EntityName' );
        
        $this::assertEquals( (string) $se, 'EntityName' );
    }
}