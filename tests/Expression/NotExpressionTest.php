<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\NotExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\NotExpression
 */
class NotExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $se = new NotExpression(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $se,
            'not mock expression'
        );
    }
}