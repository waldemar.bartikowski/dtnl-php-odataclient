<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\ListExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\ListExpression
 */
class ListExpressionTest extends TestCase {

    public function testConstruction() : void {
        $se = new ListExpression( [ 'a', 'b', 'c', ] );
        $this::assertEquals( (string) $se, 'a,b,c' );
    }

    public function testEmptyList() : void {
        $se = new ListExpression( [] );
        $this::assertEquals( (string) $se, '' );
    }

    public function testOneElementList() : void {
        $se = new ListExpression( [ 'a' ] );
        $this::assertEquals( (string) $se, 'a' );
    }

    public function testExpressionElement() : void {
        
        $se = new ListExpression( [
            new MockExpression(),
            new MockExpression(),
        ] );

        $this::assertEquals(
            (string) $se,
            'mock expression,mock expression'
        );
    }
}