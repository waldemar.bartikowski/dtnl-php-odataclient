<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\ParensExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\ParensExpression
 */
class ParensExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $expression = new ParensExpression(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $expression,
            '(mock expression)'
        );
    }
}