<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\RawExpression;

/**
 * @covers \DTNL\OdataClient\Expression\RawExpression
 */
class RawExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $se = new RawExpression( 'Value eq 0' );
        
        $this::assertEquals(
            (string) $se,
            'Value eq 0'
        );
    }
}