<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\StringExpression;

/**
 * @covers \DTNL\OdataClient\Expression\StringExpression
 */
class StringExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $expression = new StringExpression( "It's a trap!" );
        
        $this::assertEquals(
            (string) $expression,
            "'It''s a trap!'"
        );
    }
}