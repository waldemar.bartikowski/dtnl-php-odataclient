<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Expression;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Expression\OrExpression;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Expression\OrExpression
 */
class OrExpressionTest extends TestCase {

    public function testConstruction() : void {
        
        $se = new OrExpression(
            new MockExpression(),
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $se,
            '(mock expression or mock expression)'
        );
    }
}