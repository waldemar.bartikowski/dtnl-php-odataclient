<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;
use \DTNL\OdataClient\Parameter\SearchParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\SearchParameter
 */
class SearchParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $search = new SearchParameter( new MockExpression() );

        $this::assertEquals(
            (string) $search,
            '$search=mock expression'
        );

    }

}