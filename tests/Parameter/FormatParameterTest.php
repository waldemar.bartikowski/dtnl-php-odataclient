<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\FormatParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\FormatParameter
 */
class FormatParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $format = new FormatParameter( 'json' );
        
        $this::assertEquals(
            (string) $format,
            '$format=json'
        );
    }
}