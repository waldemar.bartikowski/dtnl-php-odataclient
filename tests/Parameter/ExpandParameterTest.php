<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\ExpandParameter;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Parameter\ExpandParameter
 */
class ExpandParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $expand = new ExpandParameter(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $expand,
            '$expand=mock expression'
        );

    }
}