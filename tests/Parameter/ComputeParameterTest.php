<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\ComputeParameter;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Parameter\ComputeParameter
 */
class ComputeParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $parameter = new ComputeParameter(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $parameter,
            '$compute=mock expression'
        );

    }
}