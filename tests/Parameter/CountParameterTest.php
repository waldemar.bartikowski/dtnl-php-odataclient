<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\CountParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\CountParameter
 */
class CountParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $count_true = new CountParameter( true );
        $count_false = new CountParameter( false );
        
        $this::assertEquals(
            (string) $count_true,
            '$count=true'
        );

        $this::assertEquals(
            (string) $count_false,
            '$count=false'
        );
    }
}