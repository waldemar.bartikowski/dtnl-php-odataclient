<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\OrderbyParameter;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Parameter\OrderbyParameter
 */
class OrderbyParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $orderby = new OrderbyParameter(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $orderby,
            '$orderby=mock expression'
        );

    }
}