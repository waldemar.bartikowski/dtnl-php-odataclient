<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\TopParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\TopParameter
 */
class TopParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $top = new TopParameter( 12 );

        $this::assertEquals(
            (string) $top,
            '$top=12'
        );

    }

}