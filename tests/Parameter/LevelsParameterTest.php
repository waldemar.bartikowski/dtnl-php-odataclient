<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\LevelsParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\LevelsParameter
 */
class LevelsParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $levels = new LevelsParameter( 12 );

        $this::assertEquals(
            (string) $levels,
            '$levels=12'
        );

    }

    public function testMax() : void {
        
        $levels = new LevelsParameter( LevelsParameter::MAX );

        $this::assertEquals(
            (string) $levels,
            '$levels=max'
        );

    }

}