<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;
use \DTNL\OdataClient\Parameter\FilterParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\FilterParameter
 */
class FilterParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $filter = new FilterParameter( new MockExpression() );

        $this::assertEquals(
            (string) $filter,
            '$filter=mock expression'
        );

    }

}