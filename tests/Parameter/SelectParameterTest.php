<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\SelectParameter;
use \DTNL\OdataClient\Tests\Mocks\MockExpression;

/**
 * @covers \DTNL\OdataClient\Parameter\SelectParameter
 */
class SelectParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $select = new SelectParameter(
            new MockExpression()
        );
        
        $this::assertEquals(
            (string) $select,
            '$select=mock expression'
        );

    }
}