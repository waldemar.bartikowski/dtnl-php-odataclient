<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Tests\Parameter;

use \PHPUnit\Framework\TestCase;
use \DTNL\OdataClient\Parameter\SkipParameter;

/**
 * @covers \DTNL\OdataClient\Parameter\SkipParameter
 */
class SkipParameterTest extends TestCase {

    public function testConstruction() : void {
        
        $parameter = new SkipParameter( 12 );

        $this::assertEquals(
            (string) $parameter,
            '$skip=12'
        );

    }

}