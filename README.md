Odata Client
============

Create the Odata Client
-----------------------

```php
$http_client = new Psr18GuzzleAdapter( new \GuzzleHttp\Client() );
$client = new OdataClient( $http_client );
```

This creates [PSR-18](https://www.php-fig.org/psr/psr-18/) HTTP Client and 
injects it into the Odata client.

Create a Request
----------------

```php
$request = new OdataRequest(
    OdataRequestInterface::GET,
    new GuzzleHttp\Psr7\Uri( 'http://localhost/service' ),
    new OdataResourcePath( 'EntityName' )
);
```

There is also a builder class that speeds up the creation of the requests:

```php
$request = ( new OdataRequestBuilder() )
    ->setServiceRoot( 'http://localhost/service' )
    ->setResourcePath( 'EntityName' )
    ->build()
;
```

Add Parameters to the Request
-----------------------------

The Request class provides a the addParameter() method to add parameters to the request:

```php
    // Limit the result to the first item
    $request->addParameter( new TopParameter( 1 ) );

    // Limit the result to certain fields
    $request->addParameter(
        new SelectParameter(
            new ListExpression(
                [ 'field1', 'field2' ]
            )
        )
    );
```

The builder speeds up things here too:

```php
$request = ( new OdataRequestBuilder() )
    ->setServiceRoot( 'http://localhost/service' )
    ->setResourcePath( 'EntityName' )
    ->setTop( 1 )
    ->setSelect(
        new ListExpression( [ 'field1', 'field2' ] )
    )
    ->build()
;

// Let the builder create the expression
$request->setSelect( [ 'field1', 'field2' ] );
```

Currently implemented parameter classes are : ComputeParameter, CountParameter, 
ExpandParameter, FilterParameter, FormatParameter, LevelsParameter, 
OrderbyParameter, SearchParameter, SelectParameter, SkipParameter, 
TopParameter

Send request
------------

To send the request, simply pass the request to the OdataClient:

```php
$response = $client->sendRequest( $request );
```

The result is a PSR-7 response message.

Expressions
-----------

Most Parameter objects expect objects implementing the ExpressionInterface 
as parameter. A ExpressionFactory class makes the creation of these Expressions
easier:

```php
use DTNL\OdataClient\Expression\ExpressionFactory as Expr;

// Results in "(age gt 12 and height gt 140)"
$filter_expression = Expr::and(
    Expr::raw( 'age gt 12' ),
    Expr::raw( 'height gt 140' )
);

// Results in "(age gt 12 or height gt 140)"
$filter_expression = Expr::or(
    Expr::raw( 'age gt 12' ),
    Expr::raw( 'height gt 140' )
);

// Results in "field1,field2"
$list_expression = Expr::list( [ 'field1', 'field2' ] );
```