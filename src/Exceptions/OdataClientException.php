<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Exceptions;

class OdataClientException extends \Exception {};