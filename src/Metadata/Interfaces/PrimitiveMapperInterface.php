<?php
declare( strict_types = 1 );
namespace DTNL\OdataClient\Metadata\Interfaces;

// http://docs.oasis-open.org/odata/odata/v4.0/odata-v4.0-part3-csdl.html

interface PrimitiveMapperInterface {
    /**
     * Undocumented function
     *
     * @param string $type
     * @param string $value
     * @return bool|int|float|string|\DateTime
     */
    public static function map( string $type, string $value );
}