<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Metadata\Exceptions;

class PrimitiveNotSupportedException extends OdataMetadataException {
    public function __construct( string $type ) {
        parent::__construct( 'Primitive type "' . $type . '" not supported.' );
    }
}