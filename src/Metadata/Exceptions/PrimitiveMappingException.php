<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Metadata\Exceptions;

class PrimitiveMappingException extends OdataMetadataException {
}