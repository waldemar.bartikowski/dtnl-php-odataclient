<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Metadata\Exceptions;

class OdataMetadataException extends \Exception {};