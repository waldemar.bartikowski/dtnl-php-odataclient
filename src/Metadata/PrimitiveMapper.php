<?php
declare( strict_types = 1 );
namespace DTNL\OdataClient\Metadata;

use DTNL\OdataClient\Metadata\Interfaces\PrimitiveMapperInterface;
use DTNL\OdataClient\Metadata\Exceptions\PrimitiveNotSupportedException;
use DTNL\OdataClient\Metadata\Exceptions\PrimitiveMappingException;

class PrimitiveMapper implements PrimitiveMapperInterface {

    /**
     * @param string $type
     * @param string $value
     * @return bool|int|float|string|\DateTime
     */
    public static function map( string $type, string $value ) {

        switch ( $type ) {

            case 'Edm.Binary':
                $data = base64_decode( $value, TRUE );
                if ( $data === false ) {
                    throw new PrimitiveMappingException(
                        'Error mapping ' . $type . ' data.'
                    );
                }
                return $data;

            case 'Edm.Boolean':
                return $value == '1' || strtolower( $value ) == 'true';
            
            case 'Edm.Byte':
                return intval( $value );
            
            case 'Edm.Date':
            case 'Edm.DateTime':
                return \DateTime::createFromFormat(
                    'Y-m-d\TH:i:s', $value
                );  
            case 'Edm.DateTimeOffset':
                return \DateTime::createFromFormat(
                    \DateTime::ATOM, $value
                );
            
            case 'Edm.Decimal':
            case 'Edm.Double':
                return floatval( $value );

            case 'Edm.Duration':
            throw new PrimitiveNotSupportedException( $type );

            case 'Edm.Guid':
                return $value;

            case 'Edm.Int16':
            case 'Edm.Int32':
            case 'Edm.Int64':
            case 'Edm.SByte':
                return intval( $value );
                
            case 'Edm.Single':
                throw new PrimitiveNotSupportedException( $type );

            case 'Edm.Stream':
                throw new PrimitiveNotSupportedException( $type );

            case '';
            case 'Edm.String':
                return $value;

            case 'Edm.TimeOfDay':
                throw new PrimitiveNotSupportedException( $type );
            
            case 'Edm.Geography':
            case 'Edm.GeographyPoint':
            case 'Edm.GeographyLineString':
            case 'Edm.GeographyPolygon':
            case 'Edm.GeographyMultiPoint':
            case 'Edm.GeographyMultiLineString':
            case 'Edm.GeographyMultiPolygon':
            case 'Edm.GeographyCollection':
            case 'Edm.Geometry':
            case 'Edm.GeometryPoint':
            case 'Edm.GeometryLineString':
            case 'Edm.GeometryPolygon':
            case 'Edm.GeometryMultiPoint':
            case 'Edm.GeometryMultiLineString':
            case 'Edm.GeometryMultiPolygon':
            case 'Edm.GeometryCollection':
                throw new PrimitiveNotSupportedException( $type );
        }

        throw new PrimitiveNotSupportedException( $type );
    }
}
