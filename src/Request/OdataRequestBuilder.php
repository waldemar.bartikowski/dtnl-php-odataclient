<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use DTNL\OdataClient\Request\OdataRequest;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestBuilderInterface;
use DTNL\OdataClient\Parameter\TopParameter;
use DTNL\OdataClient\Parameter\SkipParameter;
use DTNL\OdataClient\Parameter\SelectParameter;
use DTNL\OdataClient\Parameter\SearchParameter;
use DTNL\OdataClient\Parameter\OrderbyParameter;
use DTNL\OdataClient\Parameter\LevelsParameter;
use DTNL\OdataClient\Parameter\FormatParameter;
use DTNL\OdataClient\Parameter\FilterParameter;
use DTNL\OdataClient\Parameter\ExpandParameter;
use DTNL\OdataClient\Parameter\CountParameter;
use DTNL\OdataClient\Parameter\ComputeParameter;
use DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class OdataRequestBuilder extends AbstractOdataRequestBuilder {

    public function build() : OdataRequestInterface {
        $request = new OdataRequest(
            $this->method,
            $this->service_root,
            $this->resource_path
        );

        if ( isset( $this->compute ) ) {
            $request->addParameter(
                new ComputeParameter( $this->compute )
            );
        }

        if ( isset( $this->search ) ) {
            $request->addParameter(
                new SearchParameter( $this->search )
            );
        }

        if ( isset( $this->filter ) ) {
            $request->addParameter(
                new FilterParameter( $this->filter )
            );
        }

        if ( isset( $this->count ) ) {
            $request->addParameter(
                new CountParameter( $this->count )
            );
        }

        if ( isset( $this->orderby ) ) {
            $request->addParameter(
                new OrderbyParameter( $this->orderby )
            );
        }

        if ( isset( $this->skip ) ) {
            $request->addParameter(
                new SkipParameter( $this->skip )
            );
        }

        if ( isset( $this->top ) ) {
            $request->addParameter(
                new TopParameter( $this->top )
            );
        }

        if ( isset( $this->levels ) ) {
            $request->addParameter(
                new LevelsParameter( $this->levels )
            );
        }

        if ( isset( $this->expand ) ) {
            $request->addParameter(
                new ExpandParameter( $this->expand )
            );
        }

        if ( isset( $this->select ) ) {
            $request->addParameter(
                new SelectParameter( $this->select )
            );
        }

        if ( isset( $this->format ) ) {
            $request->addParameter(
                new FormatParameter( $this->format )
            );
        }

        return $request;
    }
}