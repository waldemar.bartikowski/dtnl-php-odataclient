<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface;

class OdataResourcePath implements OdataResourcePathInterface {

    /** @var string */
    private $path;

    function __construct( string $path ) {
        $this->path = $path;
    }

    function __toString() : string {
        return $this->path;
    }
}