<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use Psr\Http\Message\UriInterface;
use GuzzleHttp\Psr7\Uri;
use DTNL\OdataClient\Request\OdataResourcePath;
use DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestBuilderInterface;
use DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;
use DTNL\OdataClient\Expression\ExpressionFactory;

abstract class AbstractOdataRequestBuilder implements OdataRequestBuilderInterface {

    /** @var UriInterface */
    protected $service_root;

    /** @var OdataResourcePathInterface */
    protected $resource_path;

    /** @var ExpressionInterface */
    protected $compute;

    /** @var ExpressionInterface */
    protected $search;

    /** @var ExpressionInterface */
    protected $filter;

    /** @var bool  */
    protected $count;

    /** @var ExpressionInterface */
    protected $orderby;

    /** @var int */
    protected $skip;

    /** @var int */
    protected $top;

    /** @var int */
    protected $levels;

    /** @var ExpressionInterface */
    protected $expand;

    /** @var ExpressionInterface */
    protected $select;

    /** @var string */
    protected $format;

    /** @var int */
    protected $method;

    public function __construct(
      int $method = OdataRequestInterface::GET,
      string $service_root = '',
      string $resource_path = ''
    ) {
        $this->setMethod( $method );
        $this->setServiceRoot( $service_root );
        $this->setResourcePath( $resource_path );
    }

    public function setMethod( int $method ) : OdataRequestBuilderInterface {
        $this->method = $method;
        return $this;
    }

    /**
     * @param OdataResourcePathInterface|string $resource_path
     * @return OdataRequestBuilderInterface
     */
    public function setResourcePath( $resource_path ) : OdataRequestBuilderInterface {

        if ( is_string( $resource_path ) ) {
            $this->resource_path = new OdataResourcePath( $resource_path );
        } else if ( $resource_path instanceof OdataResourcePathInterface ) {
            $this->resource_path = $resource_path;
        } else {
            throw new \InvalidArgumentException(
                'Resource path must be a string or implement OdataResourcePathInterface.'
            );
        }

        return $this;
    }

    /**
     * @param UriInterface|string $service_root
     * @return OdataRequestBuilderInterface
     */
    public function setServiceRoot( $service_root )  : OdataRequestBuilderInterface {
        
        if ( is_string( $service_root ) ) {
            $this->service_root = new Uri( $service_root );
        } else if ( $service_root instanceof UriInterface ) {
            $this->service_root = $service_root;
        } else {
            throw new \InvalidArgumentException(
                'Service root must be a string or implement UriInterface.'
            );
        }

        return $this;
    }

    /**
     * @param ExpressionInterface|string $compute
     * @return OdataRequestBuilderInterface
     */
    public function setCompute( $compute ) : OdataRequestBuilderInterface {
        $this->compute = $this->getExpressionOrCreateRaw( $compute );
        return $this;
    }

    /**
     * @param ExpressionInterface|string $search
     * @return OdataRequestBuilderInterface
     */
    public function setSearch( $search ) : OdataRequestBuilderInterface {
        $this->search = $this->getExpressionOrCreateRaw( $search );
        return $this;
    }

    /**
     * @param ExpressionInterface|string $filter
     * @return OdataRequestBuilderInterface
     */
    public function setFilter( $filter ) : OdataRequestBuilderInterface {
        $this->filter = $this->getExpressionOrCreateRaw( $filter );
        return $this;
    }

    public function setCount( bool $count ) : OdataRequestBuilderInterface {
        $this->count = $count;
        return $this;
    }

    /**
     * @param ExpressionInterface|string $orderby
     * @return OdataRequestBuilderInterface
     */
    public function setOrderBy( $orderby ) : OdataRequestBuilderInterface {
        $this->orderby = $this->getExpressionOrCreateRaw( $orderby );
        return $this;
    }

    public function setSkip( int $skip ) : OdataRequestBuilderInterface {
        $this->skip = $skip;
        return $this;
    }

    public function setTop( int $top ) : OdataRequestBuilderInterface {
        $this->top = $top;
        return $this;
    }

    public function setLevels( int $levels ) : OdataRequestBuilderInterface {
        $this->levels = $levels;
        return $this;
    }

    /**
     * @param ExpressionInterface|string[] $expand
     * @return OdataRequestBuilderInterface
     */
    public function setExpand( $expand ) : OdataRequestBuilderInterface {
        $this->expand = $this->getExpressionOrCreateList( $expand );
        return $this;
    }

    /**
     * @param ExpressionInterface|string[] $select
     * @return OdataRequestBuilderInterface
     */
    public function setSelect( $select ) : OdataRequestBuilderInterface {
        $this->select = $this->getExpressionOrCreateList( $select );
        return $this;
    }

    public function setFormat( string $format ) : OdataRequestBuilderInterface {
        $this->format = $format;
        return $this;
    }

    /**
     * @param ExpressionInterface|string $expression
     * @throws \InvalidArgumentException
     * @return ExpressionInterface
     */
    private function getExpressionOrCreateRaw( $expression ) : ExpressionInterface {
        
        if ( is_string( $expression ) ) {
            return ExpressionFactory::raw( $expression );
        } else if ( $expression instanceof ExpressionInterface ) {
            return $expression;
        }
        
        throw new \InvalidArgumentException(
            'Parameter must be a string or implement ExpressionInterface.'
        );
    }

    /**
     * @param ExpressionInterface|string[] $expression
     * @throws \InvalidArgumentException
     * @return ExpressionInterface
     */
    private function getExpressionOrCreateList( $expression ) : ExpressionInterface {
        
        if ( is_array( $expression ) ) {
            return ExpressionFactory::list( $expression );
        } else if ( $expression instanceof ExpressionInterface ) {
            return $expression;
        }
        
        throw new \InvalidArgumentException(
            'Parameter must be a string or implement ExpressionInterface.'
        );
    }
}