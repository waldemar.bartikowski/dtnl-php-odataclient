<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Request;
use DTNL\OdataClient\Request\PsrRequestFactory;
use DTNL\OdataClient\Request\Interfaces\PsrRequestFactoryInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;

class PsrRequestFactory implements PsrRequestFactoryInterface {

    public static function create(
        OdataRequestInterface $odata_request,
        ?RequestInterface $request = null
    ) : RequestInterface {

        $params = $odata_request->getParameterArray();
        $query = http_build_query( $params );

        $uri = new Uri(
            (string) $odata_request->getServiceRoot()
            . (string) $odata_request->getResourcePath()
        );
        $uri = $uri->withQuery( $query );

        $method = $odata_request->getMethod();
        
        if ( is_null( $request ) ) {
            $request = new Request( $method, $uri );
        } else {
            $request = $request
                ->withMethod( $method )
                ->withUri( $uri )
            ;
        }

        if ( $odata_request->hasBody() ) {
            $request = $request->withBody( $odata_request->getBody() );
            if ( $odata_request->hasContentType() ) {
                $request = $request->withHeader(
                    'Content-Type',
                    $odata_request->getContentType()
                );
            }
        }

        return $request;
    }
}