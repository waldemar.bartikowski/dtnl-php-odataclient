<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use Psr\Http\Message\StreamInterface;
use GuzzleHttp\Psr7\Stream;
use DTNL\OdataClient\Request\Interfaces\StreamFactoryInterface;

class StreamFactory implements StreamFactoryInterface {

    const MAX_STREAM_SIZE_IN_MEMORY = 2 * 1024 * 1024; // 2 MB
    const STREAM_MODE = 'r+';

    public static function createFromString( string $string ) : StreamInterface {

        $stream = fopen(
            'php://temp/maxmemory:' . self::MAX_STREAM_SIZE_IN_MEMORY,
            self::STREAM_MODE
        );

        if ( $stream === false ) {
            throw new \RuntimeException( 'Could not create Stream from string.' );
        }

        fputs( $stream, $string );
        rewind( $stream );
        
        return new Stream( $stream, [ 'mode' => self::STREAM_MODE ] );
    }
}