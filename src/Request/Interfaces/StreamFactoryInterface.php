<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Interfaces;

use Psr\Http\Message\StreamInterface;

interface StreamFactoryInterface {

    /**
     * @param string $string
     * @return StreamInterface
     */
    public static function createFromString( string $string ) : StreamInterface;

}