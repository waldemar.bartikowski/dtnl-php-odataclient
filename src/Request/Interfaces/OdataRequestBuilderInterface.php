<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Interfaces;

use Psr\Http\Message\UriInterface;
use DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

interface OdataRequestBuilderInterface {

    public function build() : OdataRequestInterface;

    /**
     * @param integer $method
     *   Either OdataRequestInterface::GET, OdataRequestInterface::POST,
     *   OdataRequestInterface::PATCH, or OdataRequestInterface::DELETE
     * @return OdataRequestBuilderInterface
     */
    public function setMethod( int $method ) : OdataRequestBuilderInterface;

    /**
     * @param OdataResourcePathInterface|string $resource_path
     * @return OdataRequestBuilderInterface
     */
    public function setResourcePath( $resource_path ) : OdataRequestBuilderInterface;

    /**
     * @param UriInterface|string $resource_root
     * @return OdataRequestBuilderInterface
     */
    public function setServiceRoot( $resource_root ) : OdataRequestBuilderInterface;

    /**
     * @param ExpressionInterface|string $compute
     * @return OdataRequestBuilderInterface
     */
    public function setCompute( $compute ) : OdataRequestBuilderInterface;
    
    /**
     * @param ExpressionInterface|string $search
     * @return OdataRequestBuilderInterface
     */
    public function setSearch( $search ) : OdataRequestBuilderInterface;
    
    /**
     * @param ExpressionInterface|string $filter
     * @return OdataRequestBuilderInterface
     */
    public function setFilter( $filter ) : OdataRequestBuilderInterface;
    
    public function setCount( bool $count ) : OdataRequestBuilderInterface;

    /**
     * @param ExpressionInterface|string $orderby
     * @return OdataRequestBuilderInterface
     */
    public function setOrderBy( $orderby ) : OdataRequestBuilderInterface;
    public function setSkip( int $skip ) : OdataRequestBuilderInterface;
    public function setTop( int $top ) : OdataRequestBuilderInterface;
    public function setLevels( int $levels ) : OdataRequestBuilderInterface;

    /**
     * @param ExpressionInterface|string[] $expand
     * @return OdataRequestBuilderInterface
     */
    public function setExpand( $expand ) : OdataRequestBuilderInterface;

    /**
     * @param ExpressionInterface|string[] $select
     * @return OdataRequestBuilderInterface
     */
    public function setSelect( $select ) : OdataRequestBuilderInterface;

    public function setFormat( string $format ) : OdataRequestBuilderInterface;

}