<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Interfaces;

use Psr\Http\Message\UriInterface;
use Psr\Http\Message\StreamInterface;
use DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface;
use DTNL\OdataClient\Request\Exceptions\UndefinedBodyException;
use DTNL\OdataClient\Parameter\Interfaces\ParameterInterface;

interface OdataRequestInterface {

    const GET = 1;
    const POST = 2;
    const PATCH = 3;
    const PUT = 4;
    const DELETE = 5;
 
    /**
     * @param integer $method
     * @param UriInterface $service_root
     * @param OdataResourcePathInterface $resource_path
     */
    public function __construct(
        int $method,
        UriInterface $service_root,
        OdataResourcePathInterface $resource_path
    );

    /**
     * @return OdataResourcePathInterface
     */
    public function getResourcePath() : OdataResourcePathInterface;

    /**
     * @return UriInterface
     */
    public function getServiceRoot() : UriInterface;

    /**
     * @return string
     */
    public function getMethod() : string;

    /**
     * @param ParameterInterface $parameter
     * @return self
     */
    public function addParameter( ParameterInterface $parameter ) : OdataRequestInterface;

    /**
     * @return ParameterInterface[]
     */
    public function getParameters() : array;

    /**
     * Get the array of the parameters.
     * 
     * This returns the parameters as strings in an associative array:
     * 
     * [
     *      '$top' => 3,
     *      '$filter' => 'value eq 1',
     * ]
     *
     * @return string[]|array[]
     */
    public function getParameterArray() : array;

    /**
     * @param StreamInterface $stream
     * @param string $content_type
     * @return OdataRequestInterface
     */
    public function setBody( StreamInterface $stream, string $content_type ) : OdataRequestInterface;

    /**
     * @return StreamInterface
     * @throws UndefinedBodyException
     */
    public function getBody() : StreamInterface;

    /**
     * @return boolean
     */
    public function hasBody() : bool;

    /**
     * @return boolean
     */
    public function hasContentType() : bool;

    /**
     * Get Content Type.
     * 
     * The content type is meant to be used for writing request type like POST.
     *
     * @return string
     */
    public function getContentType() : string;
}