<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Interfaces;

interface OdataResourcePathInterface {

    /**
     * Return string representation of the resource path.
     *
     * @return string
     */
    public function __toString() : string;
}