<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Interfaces;

use Psr\Http\Message\RequestInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;

interface PsrRequestFactoryInterface {

    public static function create(
        OdataRequestInterface $odata_request,
        ?RequestInterface $request = null
    ) : RequestInterface;
}