<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request;

use Psr\Http\Message\UriInterface;
use Psr\Http\Message\StreamInterface;
use DTNL\OdataClient\Request\Interfaces\OdataResourcePathInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Request\Exceptions\UndefinedContentTypeException;
use DTNL\OdataClient\Request\Exceptions\UndefinedBodyException;
use DTNL\OdataClient\Request\Exceptions\InvalidServiceRootException;
use DTNL\OdataClient\Parameter\Interfaces\ParameterInterface;

class OdataRequest implements Interfaces\OdataRequestInterface {

    /** @var int */
    private $method;

    /** @var UriInterface */
    private $service_root;

    /** @var OdataResourcePathInterface */
    private $resource_path;

    /** @var ParameterInterface[]  */
    private $parameters = [];

    /** @var StreamInterface|null */
    private $body;

    /** @var string|null */
    private $content_type;

    /**
     * @param integer $method
     * @param UriInterface $service_root
     * @param OdataResourcePathInterface $resource_path
     */
    public function __construct(
        int $method,
        UriInterface $service_root,
        OdataResourcePathInterface $resource_path
    ) {
        $this->method = $method;
        $this->setServiceRoot( $service_root );
        $this->resource_path = $resource_path;
    }

    /**
     * @return string
     */
    public function getMethod() : string {
        switch ( $this->method ) {
            case self::GET : return 'GET';
            case self::POST : return 'POST';
            case self::PATCH : return 'PATCH';
            case self::PUT : return 'PUT';
            case self::DELETE : return 'DELETE';
            default:
                throw new \RuntimeException(
                    'HTTP Method not supported.'
                );
        }
    }

    /**
     * @return OdataResourcePathInterface
     */
    public function getResourcePath() : OdataResourcePathInterface {
        return $this->resource_path;
    }

    /**
     * @return UriInterface
     */
    public function getServiceRoot() : UriInterface {
        return $this->service_root;
    }

    /**
     * @param UriInterface $service_root
     * @return OdataRequestInterface
     * @throws InvalidServiceRootException
     */
    public function setServiceRoot( UriInterface $service_root ) : OdataRequestInterface {
        if ( (string) $service_root !== ''
          && substr((string) $service_root, -1 ) !== '/' ) {
            throw new InvalidServiceRootException(
                'The Service Root requires a trailing slash!'
            );
        }
        $this->service_root = $service_root;
        return $this;
    }

    /**
     * @param ParameterInterface $parameter
     * @return OdataRequestInterface
     */
    public function addParameter( ParameterInterface $parameter ) : OdataRequestInterface {
        $this->parameters[] = $parameter;
        return $this;
    }

    /**
     * @return ParameterInterface[]
     */
    public function getParameters() : array {
        return $this->parameters;
    }

    /**
     * @return string[]|array[]
     */
    public function getParameterArray() : array {
        $array = [];

        foreach ( $this->parameters as $parameter ) {

            $name = $parameter->getName();
            $value = (string) $parameter->getExpression();

            if ( isset( $array[ $name ] ) ) {
                $array[ $name ] = $this->pushValueToArray( $array[ $name ], $value );
            } else {
                $array[ $name ] = $value;
            }
        }
        return $array;
    }

    /**
     * @param StreamInterface $stream
     * @param string $content_type
     * @return OdataRequestInterface
     */
    public function setBody( StreamInterface $stream, string $content_type ) : OdataRequestInterface {
        $this->body = $stream;
        $this->setContentType( $content_type );
        return $this;
    }

    /**
     * @return StreamInterface
     */
    public function getBody() : StreamInterface {
        if ( is_null( $this->body ) ) {
            throw new UndefinedBodyException();
        }
        return $this->body;
    }

    /**
     * @return boolean
     */
    public function hasBody() : bool {
        return !is_null( $this->body );
    }

    /**
     * @param string $content_type
     * @return OdataRequestInterface
     */
    private function setContentType( string $content_type ) : OdataRequestInterface {
        $this->content_type = $content_type;
        return $this;
    }

    /**
     * @return boolean
     */
    public function hasContentType() : bool {
        return !is_null( $this->content_type );
    }

    /**
     * @return string
     * @throws UndefinedContentTypeException
     */
    public function getContentType() : string {
        if ( is_null( $this->content_type ) ) {
            throw new UndefinedContentTypeException(
                'Content Type not set.'
            );
        }
        return $this->content_type;
    }

    /**
     * @param string|array $current
     * @param string $new
     * @return string[]|array[]
     */
    private function pushValueToArray( $current, string $new ) : array {
        if ( is_array( $current ) ) {
            $array = $current;
        } else {
            $array = [ $current ];
        }
        $array[] = $new;
        return $array;
    }

    public function __toString() : string {
        return $this->resource_path . '?' . implode( '&', $this->parameters );
    }
}