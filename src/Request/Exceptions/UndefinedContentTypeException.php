<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Exceptions;

class UndefinedContentTypeException extends OdataRequestException {};