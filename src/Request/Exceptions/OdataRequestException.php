<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Request\Exceptions;

use DTNL\OdataClient\Exceptions\OdataClientException;

class OdataRequestException extends OdataClientException {};