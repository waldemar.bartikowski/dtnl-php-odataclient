<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Client\Interfaces;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;

interface OdataRequestExceptionInterface {
      public function getRequest() : RequestInterface;
      public function getResponse() : ResponseInterface;
}