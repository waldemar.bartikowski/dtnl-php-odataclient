<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Client\Interfaces;

use Psr\Http\Message\ResponseInterface;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;

interface OdataClientInterface {
    public function setApiKey( string $api_key ) : OdataClientInterface;
    public function setBasicAuth( string $user, string $password ) : OdataClientInterface;
    public function sendRequest( OdataRequestInterface $odata_request ) : ResponseInterface;
}