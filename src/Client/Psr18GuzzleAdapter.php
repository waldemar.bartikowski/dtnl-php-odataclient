<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Client;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Client\ClientInterface;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Client;

class Psr18GuzzleAdapter implements ClientInterface {

    /** @var Client */
    private $client;

    public function __construct( Client $client ) {
        $this->client = $client;
    }

    public function sendRequest( RequestInterface $request ) : ResponseInterface {
        
        try {
            $response = $this->client->send( $request );
        } catch ( BadResponseException $e ) {
            // Remove Guzzle's Exceptions, we want to handle that on our own.
            $response = $e->getResponse();
            if ( is_null( $response ) ) {
                throw new \RuntimeException(
                    'Something went terribly wrong.'
                    . ' Creating a Guzzle BadResponseException without $response '
                    . ' is deprecated.'
                );
            }
            return $response;
        }

        return $response;
    }
}