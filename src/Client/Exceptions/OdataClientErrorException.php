<?php
declare( strict_types = 1 );
namespace DTNL\OdataClient\Client\Exceptions;

use Psr\Http\Message\ResponseInterface;

class OdataClientErrorException extends AbstractOdataRequestException {
}