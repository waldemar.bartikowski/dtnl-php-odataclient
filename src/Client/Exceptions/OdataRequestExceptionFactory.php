<?php
declare( strict_types = 1 );
namespace DTNL\OdataClient\Client\Exceptions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use DTNL\OdataClient\Exceptions\OdataClientException;
use DTNL\OdataClient\Client\Exceptions\OdataServerErrorException;
use DTNL\OdataClient\Client\Exceptions\OdataClientErrorException;

class OdataRequestExceptionFactory {

    public static function create( RequestInterface $request, ResponseInterface $response ) : OdataClientException {
        
        $code = $response->getStatusCode();
        
        if ( $response->getStatusCode() >= 400
          && $response->getStatusCode() < 500 ) {
            return new OdataClientErrorException( $request, $response );
        }

        if ( $response->getStatusCode() >= 500
          && $response->getStatusCode() < 600 ) {
            return new OdataServerErrorException( $request, $response );
        }

        throw new \RuntimeException( 'Something went terribly wrong.' );
    }
}