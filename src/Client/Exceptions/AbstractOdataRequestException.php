<?php
declare( strict_types = 1 );
namespace DTNL\OdataClient\Client\Exceptions;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use DTNL\OdataClient\Exceptions\OdataClientException;
use DTNL\OdataClient\Client\Interfaces\OdataRequestExceptionInterface;

abstract class AbstractOdataRequestException
    extends OdataClientException
    implements OdataRequestExceptionInterface {

    /** @var ResponseInterface */
    private $response;

    /** @var RequestInterface */
    private $request;
    
    public function __construct(
      RequestInterface $request,
      ResponseInterface $response
    ) {    
        $this->request = $request;
        $this->response = $response;
        parent::__construct( $this->createMessage() );
    }

    private function createMessage() : string {
        return get_class( $this )
            . ': HTTP Status Code '
            . $this->response->getStatusCode()
            . '. '
            . $this->response->getBody()->getContents()
        ;
    }

    public function getRequest() : RequestInterface {
        return $this->request;
    }

    public function getResponse() : ResponseInterface {
        return $this->response;
    }
    
};