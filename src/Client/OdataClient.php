<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Client;

use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Client\ClientInterface;
use DTNL\OdataClient\Request\PsrRequestFactory;
use DTNL\OdataClient\Request\Interfaces\OdataRequestInterface;
use DTNL\OdataClient\Client\Interfaces\OdataClientInterface;
use DTNL\OdataClient\Client\Exceptions\OdataRequestExceptionFactory;
use DTNL\OdataClient\Client\Exceptions\OdataHttpRequestException;

class OdataClient implements OdataClientInterface {
    
    /** @var ClientInterface */
    private $client;

    /** @var ?string  */
    private $api_key;

    /** @var ?string */
    private $user;

    /** @var ?string */
    private $password;

    public function __construct( ClientInterface $client ) {
        $this->client = $client;
    }

    public function setApiKey( string $api_key ) : OdataClientInterface {
        $this->api_key = $api_key;
        return $this;
    }

    public function setBasicAuth( string $user, string $password ) : OdataClientInterface {
        $this->user = $user;
        $this->password = $password;
        return $this;
    }

    public function sendRequest( OdataRequestInterface $odata_request ) : ResponseInterface {

        $request = $this->prepareRequest( $odata_request );
        $response = $this->client->sendRequest( $request );

        if ( $response->getStatusCode() >= 400 ) {
            throw OdataRequestExceptionFactory::create( $request, $response );
        }

        return $response;
    }

    private function prepareRequest( OdataRequestInterface $odata_request ) : RequestInterface {
        $request = PsrRequestFactory::create( $odata_request );
        $request = $this->addHeadersToRequest( $request );
        return $request;
    }

    private function addHeadersToRequest( RequestInterface $request ) : RequestInterface {
        
        if ( isset( $this->api_key ) ) {
            $request = $request->withHeader( 'APIKey', $this->api_key );
        }

        if ( isset( $this->user ) && isset( $this->password ) ) {
            $request = $request->withHeader(
                'Authorization',
                'Basic ' . base64_encode( $this->user . ':' . $this->password )
            );
        }

        return $request;
    }
}