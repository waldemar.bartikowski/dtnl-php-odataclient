<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;
use \DTNL\OdataClient\Parameter\Interfaces\ParameterInterface;

abstract class AbstractParameter implements ParameterInterface {

    /** @var string */
    private $name;

    /** @var ExpressionInterface */
    private $expression;

    public function __construct(
        string $name,
        ExpressionInterface $expression
    ) {
        $this->name = $name;
        $this->expression = $expression;
    }

    public function getName() : string {
        return $this->name;
    }

    public function getExpression() : ExpressionInterface {
        return $this->expression;
    }

    public function __toString() : string {
        return $this->name . '=' . (string) $this->expression;
    }
}