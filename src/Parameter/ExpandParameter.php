<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class ExpandParameter extends AbstractParameter {
    
    public function __construct( ExpressionInterface $expression ) {
        parent::__construct( '$expand', $expression );
    }

}