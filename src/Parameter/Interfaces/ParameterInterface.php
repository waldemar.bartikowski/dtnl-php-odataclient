<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter\Interfaces;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

interface ParameterInterface extends ExpressionInterface {
    public function getName() : string;
    public function getExpression() : ExpressionInterface;
}