<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\RawExpression;

class AbstractStringParameter extends AbstractParameter {
    
    public function __construct( string $name, string $value ) {
        parent::__construct(
            $name,
            new RawExpression( $value )
        );
    }

}