<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\RawExpression;

class AbstractBoolParameter extends AbstractParameter {
    
    public function __construct( string $name, bool $value ) {
        parent::__construct(
            $name,
            $value
                ? new RawExpression( 'true' )
                : new RawExpression( 'false' )
        );
    }

}