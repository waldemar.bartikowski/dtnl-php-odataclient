<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class CountParameter extends AbstractBoolParameter {
    
    public function __construct( bool $value ) {
        parent::__construct( '$count', $value );
    }

}