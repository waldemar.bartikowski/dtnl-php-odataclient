<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class TopParameter extends AbstractIntParameter {
    
    public function __construct( int $value ) {
        parent::__construct( '$top', $value );
    }

}