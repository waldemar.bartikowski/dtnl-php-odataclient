<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use \DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;

class FormatParameter extends AbstractStringParameter {
    
    public function __construct( string $value ) {
        parent::__construct( '$format', $value );
    }

}