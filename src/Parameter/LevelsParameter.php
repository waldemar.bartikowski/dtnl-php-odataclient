<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Parameter;

use DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;
use DTNL\OdataClient\Expression\RawExpression;

class LevelsParameter extends AbstractParameter {

    const MAX = -1;
    
    public function __construct( int $value ) {
        
        if ( $value === self::MAX ) {
            parent::__construct(
                '$levels',
                new RawExpression( 'max' )
            );
        } else {
            parent::__construct(
                '$levels',
                new RawExpression( (string) $value )
            );
        }
    }

}