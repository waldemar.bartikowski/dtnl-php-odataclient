<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class ParensExpression implements Interfaces\ExpressionInterface {

    /** @var Interfaces\ExpressionInterface */
    private $el;

    public function __construct( Interfaces\ExpressionInterface $el ) {
        $this->el = $el;
    }

    public function __toString() : string {
        return '(' . (string) $this->el  . ')';
    }
}