<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class RawExpression implements Interfaces\ExpressionInterface {

    /** @var string */
    private $string;

    public function __construct( string $string ) {
        $this->string = $string;
    }

    public function __toString() : string {
        return $this->string;
    }
}