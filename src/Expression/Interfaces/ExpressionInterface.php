<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression\Interfaces;

interface ExpressionInterface {
    public function __toString() : string;
}