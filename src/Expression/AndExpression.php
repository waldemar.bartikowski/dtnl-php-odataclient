<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class AndExpression implements Interfaces\ExpressionInterface {

    /** @var Interfaces\ExpressionInterface */
    private $el1;

    /** @var Interfaces\ExpressionInterface */
    private $el2;

    public function __construct(
        Interfaces\ExpressionInterface $el1,
        Interfaces\ExpressionInterface $el2
    ) {
        $this->el1 = $el1;
        $this->el2 = $el2;
    }

    public function __toString() : string {
        return '(' . (string) $this->el1 . ' and ' . (string) $this->el2 . ')';
    }
}