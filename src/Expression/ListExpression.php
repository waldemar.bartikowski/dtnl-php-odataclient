<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class ListExpression implements Interfaces\ExpressionInterface {

    /** @var array */
    private $list;

    public function __construct( array $list ) {
        $this->list = $list;
    }

    public function __toString() : string {
        $list = array_map( function ( $el ) {
                return (string) $el;
            },
            $this->list
        );
        return implode( ',', $list );
    }
}