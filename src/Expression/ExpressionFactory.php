<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

use DTNL\OdataClient\Expression\Interfaces\ExpressionInterface;
use DTNL\OdataClient\Expression\EntityExpression;

class ExpressionFactory {
    
    public static function raw( string $value ) : ExpressionInterface {
        return new RawExpression( $value );
    }

    public static function string( string $value ) : ExpressionInterface {
        return new StringExpression( $value );
    }

    public static function int( int $value ) : ExpressionInterface {
        return new RawExpression( (string) $value );
    }

    public static function and(
        ExpressionInterface $expr1,
        ExpressionInterface $expr2
    ) : ExpressionInterface {
        return new AndExpression( $expr1, $expr2 );
    }

    public static function or(
        ExpressionInterface $expr1,
        ExpressionInterface $expr2
    ) : ExpressionInterface {
        return new OrExpression( $expr1, $expr2 );
    }

    public static function list( array $list ) : ExpressionInterface {
        return new ListExpression( $list );
    }

    /**
     * @param string $name
     * @param ExpressionInterface|string|null $key
     * @return ExpressionInterface
     */
    public static function entity( string $name, $key = null ) : ExpressionInterface {
        
        if ( is_null( $key ) ) {
            return new EntityExpression( $name );
        }

        if ( is_string( $key ) ) {
            $key = self::string( $key );
        } else if ( !( $key instanceof ExpressionInterface ) ) {
            throw new \RuntimeException( 'Invalid type.' );
        }

        return new EntityExpression( $name, $key );
    }
}