<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class EntityExpression implements Interfaces\ExpressionInterface {

    /** @var string */
    private $name;

    /** @var Interfaces\ExpressionInterface|null */
    private $expr;

    public function __construct(
        string $name,
        ?Interfaces\ExpressionInterface $expr = null
    ) {
        $this->name = $name;
        $this->expr = $expr;
    }

    public function __toString() : string {
        if ( is_null( $this->expr ) ) {
            return $this->name;
        } else {
            return $this->name . '(' . (string) $this->expr . ')';
        }
    }
}