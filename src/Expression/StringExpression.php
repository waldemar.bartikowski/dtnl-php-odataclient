<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\Expression;

class StringExpression implements Interfaces\ExpressionInterface {

    /** @var string */
    private $string;

    public function __construct( string $string ) {
        $this->string = $string;
    }

    public function __toString() : string {
        $escaped_string = str_replace( "'", "''", $this->string );
        return "'" . $escaped_string . "'";
    }
}