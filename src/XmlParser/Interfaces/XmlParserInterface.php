<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\XmlParser\Interfaces;

interface XmlParserInterface {
    /**
     * Parse Xml object into associative array.
     *
     * @param \SimpleXMLElement $xml
     * @return array
     */
    public static function toArray( \SimpleXMLElement $xml ) : array;
}