<?php

declare( strict_types = 1 );
namespace DTNL\OdataClient\XmlParser;

use DTNL\OdataClient\XmlParser\Interfaces\XmlParserInterface;

class XmlParser implements XmlParserInterface {

    /**
     * @param \SimpleXMLElement $xml
     * @return array
     */
    public static function toArray( \SimpleXMLElement $xml ) : array {
        return self::unwrap( $xml );
    }

    /**
     * Unwrap Xml Object recursively.
     *
     * @param \SimpleXMLElement $xml
     * @param array $parent_namespaces
     * @param string|null $current_namespace
     * @return array
     */
    private static function unwrap(
        \SimpleXMLElement $xml,
        array $parent_namespaces = [],
        ?string $current_namespace = null
    ) : array {

        $data = [];
    
        $name = $xml->getName();
        $namespaces = self::mergeNamespacesAndAddDefault(
            array_keys( $xml->getNamespaces( true ) ),
            $parent_namespaces
        );
    
        $data['@name'] = $name;
        if ( isset( $current_namespace ) && $current_namespace !== '' ) {
            $data['@namespace'] = $current_namespace;
        }
    
        $has_children = false;
        foreach ( $namespaces as $namespace ) {
            $children = $xml->children( $namespace, true );
            $attributes = $xml->attributes( $namespace );
    
            if ( is_null( $attributes ) ) {
                $data['@attributes'] = null;
            } else {
                foreach ( $attributes as $attr_name =>  $attr_value ) {
                    $data['@attributes'][ $attr_name ] = (string) $attr_value;
                }
            }
    
            if ( $children->count() > 0 ) {
                $has_children = true;
                foreach ( $children as $child ) {
                    $child_data = self::unwrap( $child, $namespaces, $namespace );
                    $child_name = $child_data['@name'];
                    $data[ $child_name ][] = $child_data;
                }
            }
        }
    
        if ( $has_children ) { return $data; }
    
        $data['content'] = (string) $xml;
    
        return $data;
    
    }

    /**
     * @param array ...$namespaces
     * @return array
     */
    private static function mergeNamespacesAndAddDefault( array ...$namespaces ) : array {
        $merged_namespaces = [ '' ];
        foreach ( $namespaces as $namespace ) {
            $merged_namespaces = array_merge( $merged_namespaces, $namespace );
        }
        $merged_namespaces = array_unique( $merged_namespaces );
        return $merged_namespaces;
    }

}